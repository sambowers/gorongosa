# Gorongosa

Gorongosa national park biomass and change maps

Contact: sam.bowers@ed.ac.uk

## Data description

Biomass and biomass change maps are produced using freely available L-band radar data from JAXA, and processed using the 'biota' tool: https://bitbucket.org/sambowers/biota/. 

Three images are provided:

1. **AGB_2007.tif**: Aboveground biomass in units of tC/ha.
2. **ChangeType_2007_2016.tif**: Change areas from 2007 - 2016. (Visually better than 2007 - 2017).
3. **ChangeType_2007_2017.tif**: Change areas from 2007 - 2017

## Change data

ChangeType*.tif datasets are classified into 7 classes:

* [0] Not forest (< 10 tC/ha)
* [1] Deforestation (Loss of > 25% biomass, over at least 0.5 ha, with biomass reduced below 10 tC/ha)
* [2] Degradation (Loss of > 25% biomass, over at least 0.5 ha, with biomass not reduced below 10 tC/ha)
* [3-6] Minor changes or (re)growth (Unlikey to be reliable in these data).

Forest is defined as areas >= 10 tC/ha.

Note that areas of seasonal flooding are frequently mis-classified as change in L-band radar imagery.

![Woody cover change near Gorongosa National Park](GIS/gorongosa_v1.png)